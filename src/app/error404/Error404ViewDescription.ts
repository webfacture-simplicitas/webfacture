import { ViewDescription, ViewRoute } from "@webfacture/simplicitas-shell";
import { Error404Component } from './error404.component';


@ViewDescription
export class Error404ViewDescription
{
    public static ViewRoute: ViewRoute = new ViewRoute("404", Error404Component);

    ViewRoute: ViewRoute = Error404ViewDescription.ViewRoute;
}