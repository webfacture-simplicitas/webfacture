import { Component } from '@angular/core';
import { ShellAngularComponent, ShellRoutingService, WellKnownRegions } from '@webfacture/simplicitas-shell';
import { HomeViewDescription } from '../home/home/HomeViewDescription';

@Component({
  selector: 'app-error404',
  templateUrl: './error404.component.html',
  styleUrls: ['./error404.component.scss']
})
export class Error404Component extends ShellAngularComponent<Error404Component> 
{
  constructor(private mNavigationService: ShellRoutingService)
  {
    super();
  }

  public async NavigateToHome(): Promise<void>
  {
    await this.mNavigationService.Navigate(HomeViewDescription.ViewRoute, WellKnownRegions.MainContent);
  }
}
