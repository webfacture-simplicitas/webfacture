import { Command, MediaQueryAnimator, ShellAnimator } from "@webfacture/simplicitas-shell";
import { ThreeStepsToSiteComponent } from "./three-steps-to-site.component";

import { gsap, TimelineMax, Expo } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
import { ScrollToPlugin } from "gsap/ScrollToPlugin";
import { DrawSVGPlugin } from "gsap/dist/DrawSVGPlugin";
import { ScrollToStep } from "src/app/animation-manager/scroll-to/ScrollToStep";

export class ThreeStepsToSiteAnimator extends MediaQueryAnimator<ThreeStepsToSiteComponent>
{
    Dispose() {    }

    InitializeAnimations(): void 
    {
        gsap.registerPlugin(ScrollTrigger);
        gsap.registerPlugin(ScrollToPlugin);
        gsap.registerPlugin(DrawSVGPlugin);

        this.mMediaQueries.SetValue(0, Command.Execute(async () => this._InitializeMobile()));
        this.mMediaQueries.SetValue(1200, Command.Execute(async () => this._InitializeDesktop()));

        if(window.innerWidth > 1200)
        {
            this._InitializeDesktop();
        }
        else
        {
            this._InitializeMobile();
        }
    }

    private _InitializeMobile()
    {
        const step1Element = document.getElementById("step-1");
        const step2Element = document.getElementById("step-2");
        const step3Element = document.getElementById("step-3");

        const step1 = this._SetupStep1(step1Element, 0);
        const step2 = this._SetupStep2Mobile(step2Element);
        const step3 = this._SetupStep3Mobile(step3Element);

        gsap.to(step1Element, 
            {
                scrollTrigger: {
                    trigger: step1Element,
                    start: "top 60%",
                    end: "bottom 15%",
                    // markers: true,
                    onEnter: () => 
                    {
                        step1.OnEnter();
                    },
                    onLeaveBack: () =>
                    {
                        step1.OnLeave();
                    },
                    onLeave: () => 
                    {
                        step1.OnLeave();
                    },
                    onEnterBack: () => 
                    {
                        step1.OnEnter();
                    }
                },
            });

        gsap.to(step2Element, 
            {
                scrollTrigger: {
                    trigger: step2Element,
                    start: "top 60%",
                    end: "bottom 15%",
                    onEnter: () => 
                    {
                        step2.OnEnter();
                    },
                    onLeaveBack: () =>
                    {
                        step2.OnLeave();
                    },
                    onLeave: () => 
                    {
                        step2.OnLeave();
                    },
                    onEnterBack: () => 
                    {
                        step2.OnEnter();
                    }
                },
            });

        gsap.to(step3Element, 
            {
                scrollTrigger: {
                    trigger: step3Element,
                    start: "top 60%",
                    end: "bottom 15%",
                    onEnter: () => 
                    {
                        step3.OnEnter();
                    },
                    onLeaveBack: () =>
                    {
                        step3.OnLeave();
                    },
                    onLeave: () => 
                    {
                        step3.OnLeave();
                    },
                    onEnterBack: () => 
                    {
                        step3.OnEnter();
                    }
                },
            });
    }

    private _InitializeDesktop()
    {
        const step1Element = document.getElementById("step-1");
        const step2Element = document.getElementById("step-2");
        const step3Element = document.getElementById("step-3");

        const step1 = this._SetupStep1(step1Element);
        const step2 = this._SetupStep2(step2Element);
        const step3 = this._SetupStep3(step3Element);

        this.ViewModel
            .ScrollToManager
            .AddPart(1)
            .AddStep(step1)
            .AddStep(step2)
            .AddStep(step3);
    }

    private _SetupStep1(step1: HTMLElement, initialDelay: number = 0.5) : ScrollToStep
    {
        const tl = new TimelineMax({paused: true}, 1);

        tl.from('#step-1-mac',
        {
            x:"+=100%",
            duration: 2,
            opacity: 0,
            ease: Expo.easeOut
        }, "+" + initialDelay);

        tl.from('#step-1-text',
        {
            duration: 2,
            opacity: 0
        }, "+" + initialDelay);

        
        tl.to('#step-1-path',
        {
            startAt: {
                drawSVG: "50% 50%"
            },
            drawSVG: "50.1% 100%",
            duration: 2,
            opacity: 1,
            ease:"power1.inOut"
        }, "+" + initialDelay);

        return new ScrollToStep("Step 1", step1, tl);
    }

    private _SetupStep2(step2: HTMLElement) : ScrollToStep
    {
        const tl = new TimelineMax({paused: true}, 1);
        const tlCustomIn = new TimelineMax({paused: true}, 1);

        const mac = document.getElementById("step-2-mac");        
        tl.from(mac,
        {
            x: -800,
            duration: 2,
            opacity: 0,
            ease: Expo.easeOut
        }, "+0.5");

        tl.from('#step-2-text',
        {
            duration: 2,
            opacity: 0
        }, "+0.5");

        tlCustomIn.to('#step-2-path',
        {
            startAt: {
                drawSVG: "84% 100%",
                opacity: 1
            },
            drawSVG: "51% 100%",
            duration: 2,
            opacity: 1,
            ease:"power1.inOut"
        }, "+0.5");

        return new ScrollToStep("Step 2", step2, tl, tlCustomIn);
    }

    private _SetupStep2Mobile(step2: HTMLElement) : ScrollToStep
    {
        const tl = new TimelineMax({paused: true}, 1);

        const mac = document.getElementById("step-2-mac");        
        tl.from(mac,
        {
            x: -800,
            duration: 2,
            opacity: 0,
            ease: Expo.easeOut
        }, "0");

        tl.from('#step-2-text',
        {
            duration: 2,
            opacity: 0
        }, "0");

        tl.to('#step-2-path',
        {
            startAt: {
                drawSVG: "0% 0%",
            },
            drawSVG: "0% 50%",
            duration: 2,
            opacity: 1,
            ease:"power1.inOut"
        }, "0");

        return new ScrollToStep("Step 2", step2, tl);
    }

    private _SetupStep3(step3: HTMLElement)
    {
        const tl = new TimelineMax({paused: true}, 1);
        const tlIn = new TimelineMax({paused: true}, 1);

        tlIn.to('#step-2-path',
        {
            startAt: {
                drawSVG: "50% 84%",
                opacity: 1
            },
            drawSVG: "84% 100%",
            duration: 1.5,
            opacity: 1,
            ease:"power1.inOut"
        }, "0");

        tlIn.to('#step-2-path',
        {
            startAt: {
                drawSVG: "84% 100%",
                opacity: 1
            },
            drawSVG: "84% 100%",
            duration: 2,
            opacity: 1,
            ease:"power1.inOut"
        }, "+1.5");

        tl.from('#step-3-mac',
        {
            x:"+=100%",
            duration: 2,
            opacity: 0.5,
            ease: Expo.easeOut
        }, "+0");

        tl.from('#step-3-text',
        {
            duration: 2,
            opacity: 0
        }, "+0.5");

        return new ScrollToStep("Step 3", step3, tl, tlIn);
    }

    private _SetupStep3Mobile(step3: HTMLElement)
    {
        const tl = new TimelineMax({paused: true}, 1);

        tl.from('#step-3-mac',
        {
            x:"+=100%",
            duration: 2,
            opacity: 0.5,
            ease: Expo.easeOut
        }, "0");

        tl.from('#step-3-text',
        {
            duration: 2,
            opacity: 0
        }, "0");

        return new ScrollToStep("Step 3", step3, tl);
    }

    GoToTop() : void
    {
        gsap.to(window, 
            {
                scrollTo: 
                {
                    y: 0, 
                    autoKill: false
                },
                duration: 1
            });
        //this.mStep1Timeline.reverse(1.5);
    }

    GoToSection(panel: Element, timelineLeaving: TimelineMax, timelineEnter: TimelineMax) : void
    {
        gsap.to(window, 
            {
                scrollTo: 
                {
                    y: panel, 
                    autoKill: false
                },
                duration: 1
            });

        timelineLeaving.reverse(1.5);
        timelineEnter.play();
    }
}