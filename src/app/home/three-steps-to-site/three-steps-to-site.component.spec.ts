import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreeStepsToSiteComponent } from './three-steps-to-site.component';

describe('ThreeStepsToSiteComponent', () => {
  let component: ThreeStepsToSiteComponent;
  let fixture: ComponentFixture<ThreeStepsToSiteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThreeStepsToSiteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreeStepsToSiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
