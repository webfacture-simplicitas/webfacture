import { Component } from '@angular/core';
import { Animator, Command, ShellAngularComponent } from '@webfacture/simplicitas-shell';
import { ScrollToManagerService } from 'src/app/animation-manager/scroll-to-manager.service';
import { ScrollToManager } from 'src/app/animation-manager/scroll-to/ScrollToManager';
import { WellKnownSiteIds } from 'src/app/WellKnownSiteIds';
import { ThreeStepsToSiteAnimator } from './ThreeStepsToSiteAnimator';
import { WellKnownScrollToIds } from '../WellKnownScrollToIds';

@Component({
  selector: 'app-three-steps-to-site',
  templateUrl: './three-steps-to-site.component.html',
  styleUrls: ['./three-steps-to-site.component.scss']
})
@Animator(new ThreeStepsToSiteAnimator())
export class ThreeStepsToSiteComponent extends ShellAngularComponent<ThreeStepsToSiteComponent> {

  public ScrollToManager: ScrollToManager;
  public NavigateToContact: Command;

  constructor(scrollToManagerService: ScrollToManagerService) 
  { 
    super();
    this.ScrollToManager = scrollToManagerService.GetScrollToManager(WellKnownSiteIds.Home);
    this.NavigateToContact = Command.Execute(async () => await this._ScrollToContact());
  }

  private async _ScrollToContact() : Promise<void>
  {
    this.ScrollToManager.ScrollToStep(this.ScrollToManager.Parts.GetValue(WellKnownScrollToIds.Contact).Steps[0])
  }

}
