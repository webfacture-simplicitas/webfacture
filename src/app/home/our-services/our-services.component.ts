import { Component, OnInit } from '@angular/core';
import { ShellAngularComponent, ShellRoutingService, WellKnownRegions } from '@webfacture/simplicitas-shell';
import { ScrollToManagerService } from 'src/app/animation-manager/scroll-to-manager.service';
import { ScrollToManager } from 'src/app/animation-manager/scroll-to/ScrollToManager';
import { ScrollToStep } from 'src/app/animation-manager/scroll-to/ScrollToStep';
import { ServicesPageViewDescription } from 'src/app/services-page/ServicesPageViewDescription';
import { WellKnownSiteIds } from 'src/app/WellKnownSiteIds';

@Component({
  selector: 'app-our-services',
  templateUrl: './our-services.component.html',
  styleUrls: ['./our-services.component.scss']
})
export class OurServicesComponent extends ShellAngularComponent<OurServicesComponent> implements OnInit {

  private readonly mScrollToManagerIndex = 2;
  private mScrollToManager : ScrollToManager;

  constructor(scrollToManagerService: ScrollToManagerService,
    private mNavigationService: ShellRoutingService) 
  { 
    super();
    this.mScrollToManager = scrollToManagerService.GetScrollToManager(WellKnownSiteIds.Home);
  }

  async Initialize() : Promise<void>
  {
    const ourServicesContainer = document.getElementById('service-container');

    this.mScrollToManager
      .AddPart(this.mScrollToManagerIndex)
      .AddStep(new ScrollToStep("Unsere Angebote", ourServicesContainer));

    await super.Initialize();
  }

async NavigateToServicesView(){
  await this.mNavigationService.Navigate(ServicesPageViewDescription.ViewRoute,WellKnownRegions.MainContent);
}

}
