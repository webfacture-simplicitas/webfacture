import { Component } from '@angular/core';
import { Animator, Command, ShellAngularComponent } from '@webfacture/simplicitas-shell';
import { Subject } from 'rxjs';
import { ScrollToManagerService } from 'src/app/animation-manager/scroll-to-manager.service';
import { ScrollToManager } from 'src/app/animation-manager/scroll-to/ScrollToManager';
import { WellKnownSiteIds } from 'src/app/WellKnownSiteIds';
import { TeamMember } from './data/TeamMember';
import { TeamViewAnimator } from './TeamViewAnimator';

@Component({
  selector: 'app-team-view',
  templateUrl: './team-view.component.html',
  styleUrls: ['./team-view.component.scss']
})
@Animator(new TeamViewAnimator())
export class TeamViewComponent extends ShellAngularComponent<TeamViewComponent>
{
  public ScrollToManager: ScrollToManager;

  private mTeam: TeamMember[] = [];
  public SelectedMemberSubject: Subject<TeamMember>;
  public SelectedMember: TeamMember;

  public SelectNextCommand: Command;
  public SelectPrevCommand: Command;

  constructor(scrollToManagerService: ScrollToManagerService) 
  { 
    super();
    this.ScrollToManager = scrollToManagerService.GetScrollToManager(WellKnownSiteIds.Home);
    this.SelectedMemberSubject = new Subject<TeamMember>();
  }

  async Initialize(): Promise<void>
  {
    await super.Initialize();
    this._SetupTeam();

    this.Disposables.push(
      this.SelectedMemberSubject.subscribe(newSelected => this.SelectedMember = newSelected));

    this.SelectNextCommand = Command.Execute(() => this._SelectNextMember());
    this.SelectPrevCommand = Command.Execute(() => this._SelectPrevMember());

    this.SelectedMemberSubject.next(this.mTeam[0]);
  }

  private _SetupTeam() : void
  {
    this.mTeam.push(
      new TeamMember(
        "Angelo Lionardo Jantscher", 
        "Informationsdesigner", 
        "Mich haben komplexe, aber gleichzeitig auch ästhetische und benutzerfreundliche Websiten sehr interessiert. "
          + "Davon gibt es im Netz leider viel zu wenige. Ich möchte mit Webfacture einzigartige Websiten designen und "
          + "die 10 Designregeln von Dieter Rams dabei beherzigen.", 
        "Ich liebe es Bücher zu kaufen, aber es fällt mir schwer sie auch zu lesen.",
        "./assets/OPIES/PNG/WF_S_OPIE_JAN_2021.png"));

    this.mTeam.push(
      new TeamMember(
        "Laura Binder", 
        "Software Developer", 
        "Meine Motivation für Webfacture ist es für jeden Kunden die passende Website zu erstellen und dabei die "
          + "Einzigartigkeit eines jeden Unternehmens hervorzuheben.", 
        "Suppe ist mein Leibgericht - meistens esse ich sie zum Frühstück.",
        "./assets/OPIES/PNG/WF_S_OPIE_BIN_2021.png"));

      this.mTeam.push(
        new TeamMember(
          "Sonja Fauland", 
          "Marketing, Kundenkontakt", 
          "Meine Motivation für Webfacture kommt daher, dass ich allen Unternehmen ermöglichen möchte, eine einzigartige "
            + "Website zu haben. Auch möchte ich lange bestehenden, alteingesessenen Unternehemn die Chance auf eine moderne"
            + "und zeitgerechte Website ermöglichen.", 
          "Ich trinke täglich mehr Kaffee als Wasser.",
          "./assets/OPIES/PNG/WF_S_OPIE_FAU_2021.png"));

    this.mTeam.push(
        new TeamMember(
          "Stefan König", 
          "Software Developer", 
          "Mich hat das Programmieren und besonders das Programmieren von Webseiten immer schon gefallen und mit "
            + "meinen Kenntnissen möchte ich einzigartige Websites für Unternehmen erstellen. ", 
          "Ich nehme mir vor in der Woche 3x Sport zu machen, gehe aber 3x mehr zum Kühlschrank.",
          "./assets/OPIES/PNG/WF_S_OPIE_KOE_2021.png"));

    this.mTeam.push(
      new TeamMember(
        "Daniel Pastollnigg", 
        "Lead Developer", 
        "Das Gefühl sich durch eine Webseite zu klicken, bei der jedes Scrollen, jeder Mausklick, einfach jede Aktion perfekt"
          + " durchdacht ist, ist einfach unbeschreiblich.  Solch ein Gefühl bei Besuchern unserer Webseiten auszulösen ist mein"
          + " Ziel und zugleich unsere Aufgabe als Webfacture Team.", 
        "Während meiner Schulzeit wollte ich alles, nur keine Webseiten erstellen.",
        "./assets/OPIES/PNG/WF_S_OPIE_PAS_2021.png"));
  }

  private async _SelectNextMember() : Promise<void>
  {
    const currentSelectedIndex = this.mTeam.indexOf(this.SelectedMember);
    if(currentSelectedIndex + 1 < this.mTeam.length)
    {
      this.SelectedMemberSubject.next(this.mTeam[currentSelectedIndex + 1])
    }
    else
    {
      this.SelectedMemberSubject.next(this.mTeam[0]);
    }
  }

  private async _SelectPrevMember() : Promise<void>
  {
    const currentSelectedIndex = this.mTeam.indexOf(this.SelectedMember);
    if(currentSelectedIndex - 1 >= 0)
    {
      this.SelectedMemberSubject.next(this.mTeam[currentSelectedIndex - 1])
    }
    else
    {
      this.SelectedMemberSubject.next(this.mTeam[this.mTeam.length - 1]);
    }
  }
}
