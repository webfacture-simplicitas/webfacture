

export class TeamMember
{
    public Name: string;
    public Role: string;

    public Motivation: string;
    public FunFact: string;
    public ImagePath: string;

    constructor(name: string, role: string, motivation: string, funFact: string, imagePath: string)
    {
        this.Name = name;
        this.Role = role;
        this.Motivation = motivation
        this.FunFact = funFact;
        this.ImagePath = imagePath;
    }
}