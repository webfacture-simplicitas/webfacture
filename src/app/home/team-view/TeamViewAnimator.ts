import { ShellAnimator } from '@webfacture/simplicitas-shell';
import { TeamViewComponent } from './team-view.component';

import { gsap, TimelineMax } from "gsap";
import { DrawSVGPlugin } from "gsap/dist/DrawSVGPlugin";
import { TextPlugin } from "gsap/dist/TextPlugin";

import { fromEvent } from "rxjs";
import { filter } from "rxjs/operators";
import { TeamMember } from './data/TeamMember';
import { ScrollToStep } from 'src/app/animation-manager/scroll-to/ScrollToStep';

export class TeamViewAnimator extends ShellAnimator<TeamViewComponent>
{
    private readonly mScrollToManagerIndex = 4;

    private mRightArrowAnimation: TimelineMax;
    private mLeftArrowAnimation: TimelineMax;
    private mShowAnimation: TimelineMax;
    private mTransitionAnimation: TimelineMax;

    InitializeAnimations(): void 
    {
        gsap.registerPlugin(DrawSVGPlugin);
        gsap.registerPlugin(TextPlugin);

        this._SetupShowAnimation();

        this.ViewModel.ScrollToManager
        .AddPart(this.mScrollToManagerIndex)
        .AddStep(new ScrollToStep(
            'Das Team', 
            document.getElementById('team-view-container'),
            this.mShowAnimation));

        this._SetupLeftArrow();
        this._SetupRightArrow();
        this._SetupTransitionAnimation();

        this.ViewModel.SelectedMemberSubject
            .subscribe(x => this._AnimateTransition(x));
    }

    private _SetupTransitionAnimation()
    {
        const fadeInElements = [
            document.getElementById('motivation-header'),
            document.getElementById('member-description'),
            document.getElementById('fun-fact-header'),
            document.getElementById('member-fun-fact'),
        ];

        const opieContainer = document.getElementById('member-opie-container');
        let startTime = 0.25;
        this.mTransitionAnimation = new TimelineMax({paused: true});

        this.mTransitionAnimation.to(opieContainer,
            {
                opacity: 0,
                duration: 0,
            }, 0);
        this.mTransitionAnimation.to(opieContainer,
            {
                opacity: 1,
                duration: 1,
            }, 0.5);

        for (const element of fadeInElements) 
        {
            this.mTransitionAnimation.to(element,
                {
                    opacity: 0,
                    duration: 0
                }, 0);

            this.mTransitionAnimation.to(element,
                {
                    opacity: 1,
                    duration: 1,
                }, startTime);
            startTime += 0.25;
        }
    }

    private _AnimateTransition(newSelected: TeamMember)
    {
        const role = document.getElementById('member-role');
        const name = document.getElementById('member-name');

        this.mTransitionAnimation.restart();

        gsap.to(role, 
            {
                text: newSelected.Role,
                duration: 1,
                opacity: 1
            });
    
        gsap.to(name, 
            {
                text: newSelected.Name,
                duration: 1,
                opacity: 1
            });
    }

    private _SetupShowAnimation(): void 
    {
        this.mShowAnimation = new TimelineMax({paused: true});
        const fadeInElements = [
            document.getElementById('member-role'),
            document.getElementById('member-name'),
            document.getElementById('motivation-header'),
            document.getElementById('member-description'),
            document.getElementById('fun-fact-header'),
            document.getElementById('member-fun-fact'),
            document.getElementById('member-opie-container')
        ];

        let startTime = 0.25;

        for (const element of fadeInElements) 
        {
            this.mShowAnimation.to(element,
                {
                    opacity: 0,
                    duration: 0
                }, 0);

            this.mShowAnimation.to(element,
                {
                    opacity: 1,
                    duration: 1,
                }, startTime);
            startTime += 0.25;
        }
    }

    private _SetupLeftArrow() : void
    {
        this.mLeftArrowAnimation = new TimelineMax({paused: true});
        
        this.mLeftArrowAnimation.to('#left-arrow-svg',
        {
            scale: 1.2,
            duration: 0.2,
        });

        const leftArrow = document.getElementById('left-arrow-svg');

        fromEvent(leftArrow, 'mouseover')
            .pipe((filter(_ => !this.mLeftArrowAnimation
                .isActive() 
                            || this.mLeftArrowAnimation.reversed())))
            .subscribe(_ => this._MouseOverLeft());

        fromEvent(leftArrow, 'mouseout')
            .pipe((filter(_ => !this.mLeftArrowAnimation.isActive() 
                            || !this.mLeftArrowAnimation.reversed())))
            .subscribe(_ => this._MouseOutLeft());
    }

    private _SetupRightArrow(): void 
    {
        const rightArrow = document.getElementById('right-arrow-svg');

        this.mRightArrowAnimation = new TimelineMax({paused: true});
        this.mRightArrowAnimation.to(rightArrow,
            {
                scale: 1.2,
                duration: 0.2,
            });

        fromEvent(rightArrow, 'mouseover')
            .pipe((filter(_ => !this.mRightArrowAnimation.isActive() 
                            || this.mRightArrowAnimation.reversed())))
            .subscribe(_ => this._MouseOverRight());

        fromEvent(rightArrow, 'mouseout')
            .pipe((filter(_ => !this.mRightArrowAnimation.isActive() 
                            || !this.mRightArrowAnimation.reversed())))
            .subscribe(_ => this._MouseOutRight());
    }

    private _MouseOverRight()
    {
        this.mRightArrowAnimation.restart();
    }

    private _MouseOutRight()
    {
        this.mRightArrowAnimation.reverse();
    }

    private _MouseOverLeft()
    {
        this.mLeftArrowAnimation.restart();
    }

    private _MouseOutLeft()
    {
        this.mLeftArrowAnimation.reverse();
    }
    
}