import { Component } from '@angular/core';
import { ShellAngularComponent } from '@webfacture/simplicitas-shell';
import { ScrollToManagerService } from 'src/app/animation-manager/scroll-to-manager.service';
import { ScrollToManager } from 'src/app/animation-manager/scroll-to/ScrollToManager';
import { ScrollToStep } from 'src/app/animation-manager/scroll-to/ScrollToStep';
import { WellKnownSiteIds } from 'src/app/WellKnownSiteIds';

@Component({
  selector: 'app-landing-section',
  templateUrl: './landing-section.component.html',
  styleUrls: ['./landing-section.component.scss']
})
export class LandingSectionComponent extends ShellAngularComponent<LandingSectionComponent>
{

  private mScrollToManager: ScrollToManager;

  constructor(scrollToManagerService: ScrollToManagerService)
  {
    super();
    this.mScrollToManager = scrollToManagerService.GetScrollToManager(WellKnownSiteIds.Home);
  }

  async Initialize(): Promise<void>
  {
    await super.Initialize();

    const step = new ScrollToStep("Home", document.getElementById('landing-section-container'));

    if(this.mScrollToManager.CurentStep === null)
      this.mScrollToManager.CurentStep = step;

    this.mScrollToManager.AddPart(0)
      .AddStep(step);
  }

}
