import { Component, ViewEncapsulation } from '@angular/core';
import { ShellAngularComponent } from '@webfacture/simplicitas-shell';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ReCaptchaService } from 'angular-recaptcha3';

@Component({
  selector: 'app-contact-view',
  templateUrl: './contact-view.component.html',
  styleUrls: ['./contact-view.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ContactViewComponent extends ShellAngularComponent<ContactViewComponent>
{
  public SiteKey: '6LezBPEaAAAAAEDGns5_P61LH4I72_LNfUg8A8vm';
  public SecretKey: '';

  public aFormGroup: FormGroup;

  public Email = new FormControl('', [Validators.required, Validators.email]);
  public ForName = new FormControl('', [Validators.required]);
  public SurName = new FormControl('', [Validators.required]);

  public Message: string = '';

  constructor(private mFormBuilder: FormBuilder,
    private recaptchaService: ReCaptchaService,
    private mHttpClient: HttpClient) 
  { 
    super();
  }

  async Initialize() : Promise<void>
  {
    this.aFormGroup = this.mFormBuilder.group({
      Email: this.Email,
      ForName: this.ForName,
      SurName: this.SurName,
    });

    await super.Initialize();
  }

  async AfterViewInit() : Promise<void>
  {
    await super.AfterViewInit();
    
  }

  public async OnSubmit() : Promise<void>
  {
    const token = await this.recaptchaService.execute({action: 'login'});
    const recaptchaValid = await this._IsRecaptchaSuccessful(token);

    if(!this.aFormGroup.invalid && recaptchaValid)
    {
      alert('All Fields valid ');
    }
  }

  private async _IsRecaptchaSuccessful(token: string) : Promise<boolean>
  {
    try
    {
      const response = await this.mHttpClient.post("/.netlify/functions/recaptcha", JSON.stringify({token: token})).toPromise();
      if(response.hasOwnProperty('success'))
      {
        return response['success'];
      }
      return false;
    }
    catch(e)
    {
      console.log(e);
      return false;
    }
  }

  getErrorMessage() 
  {
    if (this.Email.hasError('required')) 
    {
      return '';
    }
  
    return this.Email.hasError('email') ? 'Ungültige E-Mail Adresse' : '';
  }
}
