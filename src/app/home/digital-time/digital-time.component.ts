import { Component } from '@angular/core';
import { ShellAngularComponent } from '@webfacture/simplicitas-shell';
import { ScrollToManagerService } from 'src/app/animation-manager/scroll-to-manager.service';
import { ScrollToManager } from 'src/app/animation-manager/scroll-to/ScrollToManager';
import { ScrollToStep } from 'src/app/animation-manager/scroll-to/ScrollToStep';
import { WellKnownSiteIds } from 'src/app/WellKnownSiteIds';

@Component({
  selector: 'app-digital-time',
  templateUrl: './digital-time.component.html',
  styleUrls: ['./digital-time.component.scss']
})
export class DigitalTimeComponent extends ShellAngularComponent<DigitalTimeComponent> 
{
  private readonly mScrollToManagerIndex = 3;
  private mScrollToManager : ScrollToManager;

  constructor(scrollToManagerService: ScrollToManagerService) 
  { 
    super();
    this.mScrollToManager = scrollToManagerService.GetScrollToManager(WellKnownSiteIds.Home);
  }

  async Initialize(): Promise<void> 
  {
    await super.Initialize();

    const digitalTimeContainer = document.getElementById('digitalTime-container');

    this.mScrollToManager
      .AddPart(this.mScrollToManagerIndex)
      .AddStep(new ScrollToStep("Digitales Zeitalter", digitalTimeContainer));
  }
}
