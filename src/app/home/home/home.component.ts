import { Component } from '@angular/core';
import { ShellAngularComponent } from '@webfacture/simplicitas-shell';
import { ScrollToManagerService } from 'src/app/animation-manager/scroll-to-manager.service';
import { ScrollToManager } from 'src/app/animation-manager/scroll-to/ScrollToManager';
import { ScrollToStep } from 'src/app/animation-manager/scroll-to/ScrollToStep';
import { WellKnownSiteIds } from 'src/app/WellKnownSiteIds';
import { WellKnownScrollToIds } from '../WellKnownScrollToIds';
import { fromEvent } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends ShellAngularComponent<HomeComponent>
{
  private mScrollToManager: ScrollToManager;

  constructor(scrollToManagerService: ScrollToManagerService)
  {
    super();
    this.mScrollToManager = scrollToManagerService
      .GetScrollToManager(WellKnownSiteIds.Home);
  }

  async Initialize(): Promise<void>
  {
    await super.Initialize();
    
    this._MaybeActivateScrollTo();

    this.Disposables.push(fromEvent(window, 'resize')
      .subscribe(() => this._MaybeActivateScrollTo()));

    const contactFooterContainer = document.getElementById('contact-footer-container');
    this.mScrollToManager
      .AddPart(WellKnownScrollToIds.Contact)
      .AddStep(new ScrollToStep("Kontakt", contactFooterContainer));
  }

  _MaybeActivateScrollTo()
  {
    if(window.innerWidth > 1200)
    {
      this.mScrollToManager.StartListen();
    }
    else
    {
      this.mScrollToManager.StopListen();
    }
  }

  async AfterViewInit(): Promise<void>
  {
    await super.AfterViewInit();
    this.mScrollToManager.ScrollToTop();
  }

  async Destroy(): Promise<void>
  {
    await super.Destroy();
    this.mScrollToManager.StopListen();
  }
}
