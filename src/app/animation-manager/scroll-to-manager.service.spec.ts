import { TestBed } from '@angular/core/testing';

import { ScrollToManagerService } from './scroll-to-manager.service';

describe('ScrollToManagerService', () => {
  let service: ScrollToManagerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ScrollToManagerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
