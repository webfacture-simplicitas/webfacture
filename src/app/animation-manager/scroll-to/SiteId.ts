import { StringIdWrapper } from "@webfacture/simplicitas-shared";


export class SiteId extends StringIdWrapper<SiteId>
{
    constructor(id: string) {
        super(id);
    }
}