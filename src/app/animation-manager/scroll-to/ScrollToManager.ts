import { IScrollToPartExtender } from "./IScrollToPartExtender";
import { ScrollToPart } from "./ScrollToPart";
import { SiteId } from "./SiteId";
import { ScrollToStep } from "./ScrollToStep";
import { ScrollToPlugin } from "gsap/ScrollToPlugin";
import { gsap, Power2 } from "gsap";
import { ConnectableObservable, fromEvent, interval, Observable, Subject, Subscription } from "rxjs";
import { throttle, publish } from "rxjs/operators";
import { ScrollManager } from "src/app/util/DOM/ScrollManager";
import { DOMEventManager } from "src/app/util/DOM/DomEventManager";
import { SortedDictionary } from "@webfacture/simplicitas-shared";

export class ScrollToManager
{
    public SiteId: SiteId;
    public Parts: SortedDictionary<number, ScrollToPart>;

    public CurrentStepChanged: Observable<ScrollToStep>;
    public CurentStep: ScrollToStep;

    private mWheelObservable: ConnectableObservable<WheelEvent>;
    private mWheelSubscription: Subscription;
    private mCurrentStepSubject: Subject<ScrollToStep>;

    constructor(siteId: SiteId)
    {
        gsap.registerPlugin(ScrollToPlugin);
        this.SiteId = siteId;
        this.Parts = new SortedDictionary<number, ScrollToPart>((a, b) => a-b);
        this.CurentStep = null;

        this.mWheelObservable = publish<WheelEvent>()(fromEvent<WheelEvent>(document, 'wheel'));
        
        this.mWheelSubscription = this.mWheelObservable
            .pipe(throttle(() => interval(250)))
            .subscribe(evt => this._MouseWheel(evt));

        DOMEventManager.SiteReloaded
            .subscribe(() =>this.ScrollToStep(this.CurentStep));

        this.mCurrentStepSubject = new Subject();
        this.CurrentStepChanged = this.mCurrentStepSubject.asObservable();
    }

    public AddPart(index: number) : IScrollToPartExtender
    {
        const scrollToPart = new ScrollToPart();
        this.Parts.SetValue(index, scrollToPart);
        return scrollToPart;
    }

    public ScrollToTop()
    {
        this.ScrollToStep(this.Parts.Values()[0].Steps[0]);
    }

    public ScrollToStep(step: ScrollToStep)
    {
        this.mWheelSubscription?.unsubscribe();
        this.CurentStep?.OnLeave();
        this.mCurrentStepSubject.next(step);

        gsap.to(window, 
            {
                scrollTo: 
                {
                    y: step.Element, 
                    autoKill: false,
                },
                duration: 1,
                ease: Power2.easeInOut,
                onComplete: () => 
                {
                    this.CurentStep = step;
                    this.mWheelSubscription = this.mWheelObservable.connect();
                }
            });
        step.OnEnter();
    }

    public StartListen()
    {
        ScrollManager.DisableScrolling();
        this.mWheelSubscription = this.mWheelObservable.connect();
    }

    public StopListen()
    {
        ScrollManager.EnableScrolling();
        this.mWheelSubscription?.unsubscribe();
    }

    private _MouseWheel(evt: WheelEvent)
    {
        if(evt.deltaY < 0)
        {
            const prevStep = this._GetPreviousStep(this.CurentStep);
            if(prevStep != undefined)
                this.ScrollToStep(prevStep);
        }
        else
        {
            const nextStep = this._GetNextStep(this.CurentStep);
            if(nextStep != undefined)
                this.ScrollToStep(nextStep);
        }
    }

    private _GetPreviousStep(currentElement: ScrollToStep) : ScrollToStep
    {
        const stepList = this.Parts
            .Values()
            .flatMap(x => x.Steps);

        const currentIndex = stepList
            .indexOf(currentElement);

        if(currentIndex != 0)
        {
            return stepList[currentIndex-1];
        }
        return undefined;
    }

    private _GetNextStep(currentElement: ScrollToStep) : ScrollToStep
    {
        const stepList = this.Parts
            .Values()
            .flatMap(x => x.Steps);

        const currentIndex = stepList
            .indexOf(currentElement);

        if(currentIndex != stepList.length-1)
        {
            return stepList[currentIndex+1];
        }
        return undefined;
    }
}