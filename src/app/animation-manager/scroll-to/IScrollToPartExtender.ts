import { ScrollToStep } from "./ScrollToStep";


export interface IScrollToPartExtender
{
    AddStep(scrollToStep: ScrollToStep) : IScrollToPartExtender;
}