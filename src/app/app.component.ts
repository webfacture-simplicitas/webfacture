import { AfterViewInit, Component, OnInit } from '@angular/core';
import { AngularShellApplication, Command, HeaderData, HeaderDataProviderService, MenuItemFactory, ShellRoutingService, WellKnownRegions } from '@webfacture/simplicitas-shell';
import { Subject } from 'rxjs';
import { ScrollToManagerService } from './animation-manager/scroll-to-manager.service';
import { HomeViewDescription } from './home/home/HomeViewDescription';
import { MenuComponent } from './menu/menu.component';
import { ProjectPageViewDescription } from './projects-page/ProjectsPageViewDescription';
import { ServicesPageViewDescription } from './services-page/ServicesPageViewDescription';
import { WellKnownSiteIds } from './WellKnownSiteIds';
import { WellKnownScrollToIds } from './home/WellKnownScrollToIds';
import { NavigationEnd, Router } from '@angular/router';
import { delay, filter, first } from 'rxjs/operators';
import { Error404ViewDescription } from './error404/Error404ViewDescription';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends AngularShellApplication implements OnInit, AfterViewInit
{
  title = 'Webfacture';

  private mHeaderVisibility: Subject<boolean>;

  constructor (
    private mRouter: Router,
    private mScrollToManagerService: ScrollToManagerService,
    private mShellRoutingService: ShellRoutingService,
    private mHeaderDataProvider: HeaderDataProviderService,
    )
  {
    super();
    this.mHeaderVisibility = new Subject<boolean>();
    this.mHeaderDataProvider.RegisterHeaderData(new HeaderData(MenuComponent, this.mHeaderVisibility.asObservable()));
  }

  ngOnInit(): void
  {
    //alert("Device width: "+window.innerWidth+" / Device Height: "+ window.innerHeight);

    this.Initialize();

    // Disable Touch Zoom on IOS
    document.addEventListener('touchmove', (e : any) => 
    {
      if(e.scale !== undefined && e.scale !== 1) {
        e.preventDefault();
      }
    }, {passive: false});
 
    let lastHeight = window.innerHeight;
    window.addEventListener('resize', () => 
    {
      if(window.innerHeight >= lastHeight)
      {
        lastHeight = window.innerHeight;
        document.documentElement.style.setProperty('--vh', `${lastHeight * 0.01}px`);
      }
    });
    // const lastHeight = window.innerHeight * 0.01;
    // document.documentElement.style.setProperty('--vh', `${lastHeight}px`);
  }

  async ngAfterViewInit(): Promise<void>
  {
    this.mHeaderDataProvider.SetMenuData(
      [
        MenuItemFactory.Child('Home').WithNavigation(HomeViewDescription.ViewRoute, WellKnownRegions.MainContent),
        MenuItemFactory.Child('Unser Angebot').WithNavigation(ServicesPageViewDescription.ViewRoute, WellKnownRegions.MainContent),
        // MenuItemFactory.Child('Unsere Projekte').WithNavigation(ProjectPageViewDescription.ViewRoute, WellKnownRegions.MainContent),
        MenuItemFactory.Child('Kontakt').WithCommand(Command.Execute(async () => 
        { 
          if(this.mRouter.url !== '/'+HomeViewDescription.ViewRoute.Route)
          {
            const routing = await this.mShellRoutingService.Navigate(HomeViewDescription.ViewRoute, WellKnownRegions.MainContent);

            await routing.ComponentDisplayed;
          }

          const scrollToManager = this.mScrollToManagerService.GetScrollToManager(WellKnownSiteIds.Home);
          scrollToManager.ScrollToStep(scrollToManager.Parts.GetValue(WellKnownScrollToIds.Contact).Steps[0]);
        }))
      ]);

    this.mHeaderVisibility.next(false);
    
    await this.mShellRoutingService.Navigate(HomeViewDescription.ViewRoute, WellKnownRegions.MainContent);
  }

}
