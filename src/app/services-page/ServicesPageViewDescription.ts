import { ViewDescription, ViewRoute } from '@webfacture/simplicitas-shell';
import { ServicesPageComponent } from './services-page.component';


@ViewDescription
export class ServicesPageViewDescription
{
    public static ViewRoute: ViewRoute = new ViewRoute("Angebote", ServicesPageComponent);

    ViewRoute: ViewRoute = ServicesPageViewDescription.ViewRoute;
}
