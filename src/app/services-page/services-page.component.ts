import { Component } from '@angular/core';
import { ShellAngularComponent } from '@webfacture/simplicitas-shell';

@Component({
  selector: 'app-services-page',
  templateUrl: './services-page.component.html',
  styleUrls: ['./services-page.component.scss']
})
export class ServicesPageComponent extends ShellAngularComponent<ServicesPageComponent> {

  constructor() 
  { 
    super();
  }

}
