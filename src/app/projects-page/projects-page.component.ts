import { Component } from '@angular/core';
import { ShellAngularComponent } from '@webfacture/simplicitas-shell';

@Component({
  selector: 'app-projects-page',
  templateUrl: './projects-page.component.html',
  styleUrls: ['./projects-page.component.scss']
})
export class ProjectsPageComponent extends ShellAngularComponent<ProjectsPageComponent> {

  constructor() 
  {
    super();
  }

}
