import { ViewDescription, ViewRoute } from '@webfacture/simplicitas-shell';
import { ProjectsPageComponent } from './projects-page.component';


@ViewDescription
export class ProjectPageViewDescription
{
    public static ViewRoute: ViewRoute = new ViewRoute("Projekte", ProjectsPageComponent);

    ViewRoute: ViewRoute = ProjectPageViewDescription.ViewRoute;
}
