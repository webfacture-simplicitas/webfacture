

export interface IMenuAnimator 
{
    ShowMenu() : Promise<void>;

    HideMenu() : Promise<void>;

    Initialized : boolean;

    IsMobile: boolean;
}