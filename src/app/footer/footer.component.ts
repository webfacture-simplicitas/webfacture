import { Component, OnInit } from '@angular/core';
import { FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faInstagram, faFacebookSquare, IconDefinition, faApple } from '@fortawesome/free-brands-svg-icons';
import { faSquare } from '@fortawesome/free-solid-svg-icons';
import { Command, ShellAngularComponent } from '@webfacture/simplicitas-shell';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent extends ShellAngularComponent<FooterComponent> {

  public IconInstagram : IconDefinition = faInstagram;
  public IconFacebook : IconDefinition = faFacebookSquare;
  public IconSquare: IconDefinition = faSquare;

  public OpenFacebookCommand: Command;
  public OpenInstagramCommand: Command;

  public OnFacebookHoveredCommand: Command;
  public OnFacebookExitHoveredCommand: Command;

  constructor(library: FaIconLibrary) 
  { 
    super();
    library.addIcons(faInstagram, faFacebookSquare);
  }

  async Initialize(): Promise<void> 
  {
    await super.Initialize();

    this.OpenFacebookCommand = Command.Execute(
      async () => this._OpenExternalUrlInNewTab('https://www.facebook.com/Webfacture-102208698660543'));
      
    this.OpenInstagramCommand = Command.Execute(
      async () => this._OpenExternalUrlInNewTab('https://instagram.com/webfacture?igshid=trx8wm6prugx'));

    this.OnFacebookHoveredCommand = Command.Execute(
      async () => this._EnableBackground());

    this.OnFacebookExitHoveredCommand = Command.Execute(
      async () => this._DisableBackground());
  }

  private async _EnableBackground() : Promise<void>
  {
    const element = document.getElementById('facebook-background');
    element.classList.add('facebook-background-hover');
  }

  private async _DisableBackground() : Promise<void>
  {
    const element = document.getElementById('facebook-background');
    element.classList.remove('facebook-background-hover');
  }

  private async _OpenExternalUrlInNewTab(url: string) : Promise<void>
  {
    window.open(url, '_blank');
  }
}
