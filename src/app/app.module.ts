import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ReCaptchaModule } from 'angular-recaptcha3';
import { AppComponent } from './app.component';
import { SimplicitasShellModule } from '@webfacture/simplicitas-shell';
import { HomeComponent } from './home/home/home.component';
import { LandingSectionComponent } from './home/landing-section/landing-section.component';
import { ThreeStepsToSiteComponent } from './home/three-steps-to-site/three-steps-to-site.component';
import { MenuComponent } from './menu/menu.component';
import { DigitalTimeComponent } from './home/digital-time/digital-time.component';
import { TeamViewComponent } from './home/team-view/team-view.component';
import { FooterComponent } from './footer/footer.component';
import { DotNavComponent } from './components/dot-nav/dot-nav.component';
import { Error404Component } from './error404/error404.component';
import { OurServicesComponent } from './home/our-services/our-services.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContactViewComponent } from './home/contact-view/contact-view.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { ServicesPageComponent } from './services-page/services-page.component';
import { ProjectsPageComponent } from './projects-page/projects-page.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LandingSectionComponent,
    ThreeStepsToSiteComponent,
    MenuComponent,
    DigitalTimeComponent,
    TeamViewComponent,
    FooterComponent,
    DotNavComponent,
    Error404Component,
    OurServicesComponent,
    ContactViewComponent,
    ServicesPageComponent,
    ProjectsPageComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    SimplicitasShellModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    ReCaptchaModule.forRoot({
      invisible: {
          sitekey: '6LezBPEaAAAAAEDGns5_P61LH4I72_LNfUg8A8vm', 
      },
      normal: {
          sitekey: '6LezBPEaAAAAAEDGns5_P61LH4I72_LNfUg8A8vm', 
      },
      language: 'en'
  })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
