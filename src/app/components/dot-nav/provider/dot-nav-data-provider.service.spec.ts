import { TestBed } from '@angular/core/testing';

import { DotNavDataProvider } from './dot-nav-data-provider.service';

describe('DotNavDataService', () => {
  let service: DotNavDataProvider;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DotNavDataProvider);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
