
const fetcher = require('node-fetch');

const API_ENDPOINT = 'https://www.google.com/recaptcha/api/siteverify?secret=6LezBPEaAAAAACwiR5DgZpm3llGFFPy_dUpd0ssn';

exports.handler = async (event, context) => 
{
    try 
    {
        const body = JSON.parse(event.body);
        let api = API_ENDPOINT + "&response=" + body.token;

        const response = await fetcher(api, {
            method: 'POST'
        });

        const data = await response.json();

        return { statusCode: 200, body: JSON.stringify({ success: data.success }) };
    } catch (error) 
    {
      console.log(error);
      return {
        statusCode: 500,
        body: JSON.stringify({ error: 'Failed fetching data' }),
      };
    }
  };